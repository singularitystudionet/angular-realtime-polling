import {Injectable} from '@angular/core';

declare const Pusher: any;

@Injectable()
export class PusherService {
  channel;

  constructor() {
    const pusher = new Pusher('<PUSHER_KEY>', {
      cluster: 'eu',
      encrypted: true,
    });
    this.channel = pusher.subscribe('vote-channel');
  }

  public init() {
    return this.channel;
  }
}
